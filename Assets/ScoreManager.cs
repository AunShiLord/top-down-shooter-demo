﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance;
    public Text scoreText;
    public Text achivmentsText;


    private int _playerScore = 0;
    private int _enemyScore = 0;
    private float _timeSurvived = 0f;
    private List<Achivment> achivmentsSoFar = new List<Achivment>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


        // Use this for initialization
        void Start () {
        ShowScore();
	}
	
	// Update is called once per frame
	void Update () {
        _timeSurvived += Time.deltaTime;
        if (_timeSurvived > 20f)
        {
            AddAchivment(EnumAchivments.Surviving, 0);
            _timeSurvived = 0f;
        }
	}

    public void UpdateScore(bool isPlayerWin)
    {
        if (isPlayerWin)
            _playerScore++;
        else
            _enemyScore++;
        ShowScore();
    }

    private void ShowScore()
    {
        scoreText.text = "Score: " + _playerScore + " - " + _enemyScore;
    }

    public void AddAchivment(EnumAchivments achiv, int extraParam, bool addToAchivmentPool = true)
    {
        switch (achiv)
        {
            case EnumAchivments.HightRecoсhet:
                achivmentsText.text = "HIGH RECOСHET COUNT! x" + extraParam + "\n" + achivmentsText.text; break;
            case EnumAchivments.KillByRecoсhet:
                achivmentsText.text = "KILLED WITH RECOСHET! WOW! \n" + achivmentsText.text; break;
            case EnumAchivments.Surviving:
                achivmentsText.text = "OMG YOU ARE STILL ALIVE! \n" + achivmentsText.text; break;
            case EnumAchivments.Kill:
                achivmentsText.text = "HEADSHOT! \n" + achivmentsText.text; break;
            case EnumAchivments.Dead:
                _timeSurvived = 0f; break;
            default:
                break;
        }

        achivmentsSoFar.Add(new Achivment(achiv, extraParam));

        if (achiv == EnumAchivments.Dead || achiv == EnumAchivments.Kill)
        {
            //RestartLevel();
        }
    }

    private void UpdateAchivmentsBoard()
    {
        foreach(Achivment achiv in achivmentsSoFar)
        {
            AddAchivment(achiv.type, achiv.extraParam, false);
        }
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
