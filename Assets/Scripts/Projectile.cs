﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [HideInInspector]
    public bool isShotByPlayer = false;

    private TrailRenderer _trailRenderer;
    //private Vector2 _movementVelocity = Vector2.zero;
    private Rigidbody2D _rigidbody;
    private int ricoshetCount = 0;
    


    private void Awake()
    {
        // disabling trailer renderer, because it creates trail on initial position change. It will be switched On in Start()
        _trailRenderer = GetComponent<TrailRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start () {

        _trailRenderer.Clear();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player"
            || collision.gameObject.tag == "AIEnemy"
            || collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
            
        }

        if (collision.gameObject.tag == "Wall")
        {
            if (isShotByPlayer && ricoshetCount > 1)
            {
                ScoreManager.instance.AddAchivment(EnumAchivments.HightRecoсhet, ricoshetCount);
            }
        }

        if (collision.gameObject.tag == "Obstacle")
        {
            ricoshetCount++;
        }

        if (collision.gameObject.tag == "Player")
        {
            //collision.collider.enabled = false;
            ScoreManager.instance.AddAchivment(EnumAchivments.Dead, 0);
            ScoreManager.instance.UpdateScore(false);
        }

        if (collision.gameObject.tag == "AIEnemy")
        {
            //collision.collider.enabled = false;
            ScoreManager.instance.UpdateScore(true);
            if (isShotByPlayer && ricoshetCount > 0)
            {
                ScoreManager.instance.AddAchivment(EnumAchivments.KillByRecoсhet, ricoshetCount);
            }
            else if (isShotByPlayer)
            {
                ScoreManager.instance.AddAchivment(EnumAchivments.Kill, 0);
            }
        }
    }

    public void SetVelocity(Vector2 vector)
    {
        _rigidbody.velocity = vector;
    }
}
