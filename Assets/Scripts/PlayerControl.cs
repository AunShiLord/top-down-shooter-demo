﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public float rotationSpeed = 5f;

    private PhysicsMovement _physicsComponent;
    private Weapon _weapon;

	// Use this for initialization
	void Start () {
        _physicsComponent = GetComponent<PhysicsMovement>();
        _weapon = GetComponent<Weapon>();
    }
	
	// Update is called once per frame
	void Update () {

        GetMovementInput();
        RotateToMouse();
        Shoot();
        if (Input.GetButtonUp("Cancel"))
        {
            Application.Quit();
        }
    }

    private void GetMovementInput()
    {
        Vector2 movementVector = new Vector2(Input.GetAxis("Horizontal"),
                                            Input.GetAxis("Vertical"));

        _physicsComponent.SetVelocity(movementVector);
    }

    private void RotateToMouse()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        // Lerping rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    private void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _weapon.Shoot(Input.mousePosition - Camera.main.WorldToScreenPoint(gameObject.transform.position), true);
        }
    }
}
