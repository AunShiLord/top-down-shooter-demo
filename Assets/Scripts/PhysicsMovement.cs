﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsMovement : MonoBehaviour {

    public float speed = 10f;
    private Rigidbody2D _rigidBody;
    private Vector2 moveVelocity = Vector2.zero;

	// Use this for initialization
	void Start () {
        _rigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void FixedUpdate()
    {
        _rigidBody.velocity = moveVelocity;
    }

    public void SetVelocity(Vector2 velocity)
    {
        moveVelocity = velocity * speed;
    }
}
