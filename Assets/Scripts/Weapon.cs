﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public GameObject projectilePrefab;
    public float reloadTime = 0.5f;
    public float projectileSpeed = 15f;
    public Color projectileColor = Color.white;
    public Transform projectileSpawnPoint;

    private float _lastShotTime = -0.2f;

    // Use this for initialization
    private void Awake()
    {
        _lastShotTime = -reloadTime;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool Shoot(Vector3 direction, bool isShotByPlayer = false)
    {
        if (Time.time - _lastShotTime > reloadTime)
        {
            _lastShotTime = Time.time;

            // creating projectile
            GameObject projectile = Instantiate(projectilePrefab);
            projectile.transform.position = projectileSpawnPoint.position;
            projectile.GetComponent<Projectile>().isShotByPlayer = isShotByPlayer;
            projectile.GetComponent<Projectile>().SetVelocity(direction.normalized * projectileSpeed);
            Destroy(projectile, 5f);
            return true;
        }
        return false;
    }
}
