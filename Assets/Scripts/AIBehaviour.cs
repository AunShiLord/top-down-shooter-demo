﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehaviour : MonoBehaviour {

    public float speed = 1f;
    public float rotationSpeed = 1f;
    [Range(1.1f, 10f)]
    public float playerSearchTime = 3f;
    // gazePoint is changing dinamicly and represents where AI is aiming
    public Transform gazePoint;

    // player var is to let AI always know where player is
    public Transform _player;
    private List<Vector3> movementPath = new List<Vector3>();
    private Weapon _weapon;
    private Collider2D _collider;
    private Rigidbody2D _rigidbody;
    private EnumAIState _state = EnumAIState.Moving;
    private float _timeSinceLastSeenPlayer = 0f;
    private Vector2 _recoilForce = Vector2.zero;

    // Use this for initialization
    void Start () {
        _weapon = GetComponent<Weapon>();
        _collider = GetComponent<Collider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
        StartCoroutine(Shoot(Random.Range(0.7f, 1.3f)));
    }
	
	// Update is called once per frame
	void Update () {

        _timeSinceLastSeenPlayer += Time.deltaTime;
        if (_timeSinceLastSeenPlayer > playerSearchTime)
        {
            PathToPlayer();
        }
        if (movementPath.Count == 0)
        {
            RandomPath();
        }
        Move();
        Rotate();
	}

    private void FixedUpdate()
    {
        ApplyRecoil();
    }

    private void RandomPath()
    {
        movementPath = GridManager.instance.FindPath(transform.position, new Location(Random.Range(0, GridManager.instance.gridWidth), Random.Range(0, GridManager.instance.gridHeight)));
    }

    private void PathToPlayer()
    {
        movementPath = GridManager.instance.FindPath(transform.position, _player.position);
        _timeSinceLastSeenPlayer = 0;
    }

    private void Move()
    {
        if (movementPath.Count != 0 && _state == EnumAIState.Moving)
        {
            transform.transform.position = Vector3.MoveTowards(transform.transform.position,
                                                                movementPath[0],
                                                                speed * Time.deltaTime);
            if (transform.transform.position == movementPath[0])
            {
                movementPath.RemoveAt(0);
            }
        }
    }

    private void Rotate()
    {
        // Rotating Enemy. If gazePoint not null, than he will aim in that direction
        // if null, then Enemy will look forward
        // else he will simply stare nowhere
        Vector2 direction;
        if (gazePoint != null)
        {
            direction = gazePoint.position - transform.position;
        }
        else if (movementPath.Count != 0)
        {
            direction = movementPath[0] - transform.position;
        }
        else
        {
            direction = transform.position;
        }

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        // Lerping rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    private IEnumerator Shoot(float timePerShot)
    {
        while (true)
        {
            // shooting where aiming
            if (gazePoint != null && _weapon != null)
            {
                var hit = CastLineTo(gazePoint.position);

                Vector2 shootingVector = gazePoint.position - transform.position;
                if (_weapon.Shoot(shootingVector))
                {  
                    StartCoroutine(Recoil(shootingVector));
                }
                

                if (hit.collider == null || hit.collider.gameObject != gazePoint.gameObject || gazePoint != _player)
                {
                    gazePoint = null;
                }

                if (hit.collider != null && hit.collider.gameObject == _player.gameObject)
                {
                    _timeSinceLastSeenPlayer = 0f;
                }
            }
            // looking for player
            else
            {
                var hit = CastLineTo(_player.position);
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject == _player.gameObject)
                    {
                        _timeSinceLastSeenPlayer = 0f;
                        gazePoint = _player;
                    }
                    else
                    {
                        // chance to gaze at obstacle around player
                        if (Random.Range(0, 100) > 50)
                        {
                            // looking for obstacle around to shoot
                            var results = Physics2D.OverlapCircleAll(_player.position, 3f);
                            if (results.Length != 0)
                            {
                                foreach (Collider2D collider in results)
                                {
                                    if (collider.gameObject.layer == 9)
                                    {
                                        gazePoint = collider.transform;
                                        break;
                                    }
                                }                                
                            }
                        }
                        
                    }
                }

            }

            yield return new WaitForSeconds(timePerShot);
        }
    }

    private IEnumerator Recoil(Vector2 shootingVector)
    {
        _state = EnumAIState.Shooting;
        _recoilForce = shootingVector.normalized * -100;
        yield return new WaitForSeconds(0.3f);
        _state = EnumAIState.Moving;
        _rigidbody.velocity = Vector2.zero;
    }

    private void ApplyRecoil()
    {
        if (_recoilForce != Vector2.zero)
        {
            _rigidbody.AddForce(_recoilForce);
            _recoilForce = Vector2.zero;
        }
    }

    private RaycastHit2D CastLineTo(Vector2 position)
    {
        _collider.enabled = false;
        var hit = Physics2D.Linecast(transform.position, position);
        _collider.enabled = true;
        return hit;
    }

    private void OnDrawGizmos()
    {
        if (gazePoint != null)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, gazePoint.position);
            if (gazePoint == null)
            {
                Gizmos.DrawWireSphere(_player.position, 2f);
            }
            
        }
    }
}
