﻿// Autor: Vladimir A'Shi Kuzmin

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour
{

    public static GridManager instance;

    public float offset;
    public float startOffset;
    public int gridWidth;
    public int gridHeight;

    private SquareGrid _grid;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        UpdateGrid();
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UpdateGrid()
    {
        _grid = null;
        _grid = new SquareGrid(0, 0, gridWidth, gridHeight);
        _grid.BuildGrid(transform.position, startOffset, offset);
    }

    public List<Vector3> FindPath(Vector3 from, Vector3 to)
    {
        Location fromL = new Location(from - transform.position);
        Location toL = new Location(to - transform.position);
        return FindPath(fromL, toL);
    }

    public List<Vector3> FindPath(Vector3 from, Location to)
    {
        Location fromL = new Location(from - transform.position);
        return FindPath(fromL, to);
    }

    public List<Vector3> FindPath(Location from, Location to)
    {
        AStarSearch aStarSearch = new AStarSearch(_grid, from, to);
        var locPath = aStarSearch.GetPath();
        // transforming to Vector3
        List<Vector3> vectorPath = new List<Vector3>();
        foreach (Location loc in locPath)
        {
            vectorPath.Add(new Vector3(transform.position.x + startOffset + offset * loc.x,
                                        transform.position.y + startOffset + offset * loc.y,
                                        transform.position.z));
        }
        return vectorPath;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 1f);
    }

    private void OnDrawGizmosSelected()
    {
        
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                if (_grid != null)
                {
                    if (_grid.passablePaths[x, y])
                        Gizmos.color = Color.green;
                    else
                        Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                Gizmos.DrawWireSphere(new Vector3(transform.position.x + startOffset + (x * offset),
                    transform.position.y + startOffset + (y * offset),
                    transform.position.z), 0.1f);
            }
        }
    }
}
