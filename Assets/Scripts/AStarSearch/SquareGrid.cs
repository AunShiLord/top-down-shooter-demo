﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SquareGrid
{
    public int x, y, width, height;

    public static readonly Location[] DIRS = new[] {
        new Location(1, 0), // to right of tile
        new Location(0, -1), // below tile
        new Location(-1, 0), // to left of tile
        new Location(0, 1), // above tile
        new Location(1, 1), // diagonal top right
        new Location(-1, 1), // diagonal top left
        new Location(1, -1), // diagonal bottom right
        new Location(-1, -1) // diagonal bottom left
      };

    public bool[,] passablePaths;

    public SquareGrid(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        passablePaths = new bool[width, height];
    }

    // Check if a location is within the bounds of this grid.
    public bool InBounds(Location id)
    {
        return (x <= id.x) && (id.x < width) && (y <= id.y) && (id.y < height);
    }

    public float Cost(Location a, Location b)
    {
        if (AStarSearch.Heuristic(a, b) == 2f)
        {
            return Mathf.Sqrt(2f);
        }
        return 1;
    }

    // Building grid over given gameObject and setted offset
    // starting from bottom left corner
    public void BuildGrid(Vector2 startPosition, float startOffset, float offset)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector2 placeToCast = startPosition + new Vector2(startOffset + x * offset, startOffset + y * offset);
                Collider2D collidedObject = Physics2D.OverlapCircle(placeToCast, 0.3f);
                // TODO: Find another way to check whether is this unpassable object. check by tag?
                // if collided object is not null and on layer 9 (Obstacle) than this point of the grid is not passable
                passablePaths[x, y] = collidedObject == null || collidedObject.gameObject.layer != 9;
            }
        }
    }

    // !!!

    // Check the tiles that are next to, above, below, or diagonal to
    // this tile, and return them if they're within the game bounds and passable
    public IEnumerable<Location> Neighbors(Location id)
    {
        foreach (var dir in DIRS)
        {
            Location next = new Location(id.x + dir.x, id.y + dir.y);
            if (InBounds(next) && passablePaths[next.x, next.y])
            {
                yield return next;
            }
        }
    }
}
