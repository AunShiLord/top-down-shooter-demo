﻿using UnityEngine;
using System.Collections;

public struct Location
{
    public readonly int x, y;

    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Location(Vector2 vector)
    {
        this.x = (int)vector.x;
        this.y = (int)vector.y;
    }

    public override bool Equals(object obj)
    {
        if (!(obj is Location))
            return false;

        Location loc = (Location)obj;
        return (loc.x == this.x && loc.y == this.y);
    }

    public override int GetHashCode()
    {
        return y.GetHashCode() ^ x.GetHashCode();
    }

}
